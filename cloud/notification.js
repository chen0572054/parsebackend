var Parse = require('parse-cloud-express').Parse;

Parse.Cloud.define("Notification", function(request, response) {
  // console.log("sending response " + request.params.modspotId);
  // var query = new Parse.Query(request.user)
  console.log('create notification command!');

  response.success('Create Notification!');  
  
}); 

function saveNotification(notification, callback) {
  var Notification = Parse.Object.extend("Notification");
  var newNotification = new Notification();
  newNotification.set("senderId", notification.senderId);
  newNotification.set("receiverId", notification.receiverId);  
  newNotification.set("createTime", notification.createTime);  
  newNotification.set("updateTime", Date.parse(new Date()));  
  newNotification.set("postId", notification.postId);
  newNotification.set("category", notification.category);
  newNotification.set("status", notification.status);
  newNotification.set("badge", 'Increment');
  newNotification.set("sound", 'Default');
  newNotification.set("alert", notification.content);

  newNotification.save(null, {
    success: function(theNotification) {
      console.log('notification saved: ' + theNotification);
      callback(theNotification);
    },
    error: function(theNotification, error) {
      console.log('notification save failed: ' + error);
      callback(theNotification, error);
    }
  });
}

function queryNotification(notification, callback) {
  Parse.Cloud 
  var Notification = Parse.Object.extend("Notification");
  var query = new Parse.Query(GameScore);
  query.equalTo(notification.key, notification.value);
  query.find({
    success: function(results) {
      console.log('found results' + results);
      callback(null, results);
    },
    error: function(error) {
      callback(error);
    }
  });
  
}

function sendNotification(notification, callback) {


}



Parse.Cloud.beforeSave('Notification', function(request, response) {
  console.log('Ran beforeSave on objectId: ' + request.object.id);
  response.success();

});

Parse.Cloud.afterSave('Notification', function(request, response) {
  console.log('Ran afterSave on objectId: ' + request.object.id);

});

Parse.Cloud.beforeDelete('TestObject', function(request, response) {
  console.log('Ran beforeDelete on objectId: ' + request.object.id);
  response.success();
});

Parse.Cloud.afterDelete('TestObject', function(request, response) {
  console.log('Ran afterDelete on objectId: ' + request.object.id);
});

