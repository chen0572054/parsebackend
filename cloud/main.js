var Parse = require('parse-cloud-express').Parse;

var express = require('express');
var app = express(); // the main app

const UNREAD_NOTIFICATION_LIMIT = 5;
const NOTIFICATION = "Notification";
const STATUS = "status";
const RECEIVER_ID = "receiverId";
const CREATE_TIME = "createTime";
const UPDATE_TIME = "updateTime";
const STATUS_READ = 4;
const MODSPOT_ID = 'modspot_id';
const OLD_NOTIFY_THRESHOLD = 60 * 60 * 24;

app.get('/custmize', function (req, res) {
    console.log('test endpoint'); // /admin
    res.json({ok: "request received"});
});
/*
  Notification activity status:
 0: unknown status
 1: sender sent
 2: receiver delivered but not viewed: active and saved at Parse
 4: receiver delivered and viewed
 8: receiver deleted

 functions:
 1. send a notificaion -> get delivered
 2. confirm delivered
 3. 
*/
// ==== this is a test endpoint =======
Parse.Cloud.define("notify", function(request, response) {
  Parse.initialize("PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2",
    "pEtgTrsGsQKeJzXO4fuxsAL8rZO3svT2ehytb9G5",
    "qmDSAXnLGf6RT16bG1vXqNcmXNe0POaK0bGrtXmV");

  console.log("getting request notification " + JSON.stringify(notification));

  saveNotification(notification, function(error, notification) {
    if(error) {
      console.log('error is ' + JSON.stringify(error));
      response.error(error);
    } else {
      console.log('notification created: ' + JSON.stringify(notification));
      response.success(notification);
    }
  });
  
}); 

function saveNotification(notification, callback) {
    Parse.initialize("PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2",
    "pEtgTrsGsQKeJzXO4fuxsAL8rZO3svT2ehytb9G5",
    "qmDSAXnLGf6RT16bG1vXqNcmXNe0POaK0bGrtXmV");
    console.log('Begin saving notification');
    var Notification = Parse.Object.extend("Notification");
    var newNotification = new Notification();
    if(typeof(notification.createTime) == "string") {
        notification.createTime = Date.parse(notification.createTime);
        console.log('create time is ' + notification.createTime);
    }
    if(typeof(notification.updateTime) == "string") {
        notification.updateTime = Date.parse(notification.updateTime);
    }
    if(notification.senderId != undefined) {
        newNotification.set("senderId", notification.senderId);    
    }
    if(notification.receiverId != undefined) {
        newNotification.set(RECEIVER_ID, notification.receiverId);    
    }
    if(notification.createTime != undefined) {
        newNotification.set(CREATE_TIME, notification.createTime);    
    }
    if(notification.updateTime != undefined) {
        newNotification.set(UPDATE_TIME, notification.updateTime);    
    // newNotification.set("updateTime", new Date());   
    }
    
    if(notification.postId != undefined) {
        newNotification.set("postId", notification.postId);    
    }
    if(notification.senderName != undefined) {
        newNotification.set("senderName", notification.senderName);    
    }
    if(notification.category != undefined) {
        newNotification.set("category", notification.category);    
    }
    if(notification.status != undefined) {
        newNotification.set(STATUS, notification.status);    
    }
    if(notification.badge != undefined) {
        newNotification.set("badge", notification.badge);    
    } else {
        newNotification.set("badge", "Increment");
    }
    if(notification.sound != undefined) {
        newNotification.set("sound", notification.sound);    
    } else {
        newNotification.set("sound", "Default");    
    }
    if(notification.alert != undefined) {
        newNotification.set("alert", notification.alert);    
    }
    console.log('the new notification: ' + JSON.stringify(newNotification));
    newNotification.save(null, {
        success: function(theNotification) {
            console.log('  notification saved: ' 
                        + JSON.stringify(theNotification));
            callback(null, theNotification);
        },
        error: function(error) {
            console.log('notification save failed: ' + JSON.stringify(error));
            callback(error);
        }
    });
}

function convertNotificationForAlertMessage(notification) {
    var category = notification.category
    if(category < 100) { // like
        return notificaion.username + ' liked your post';
    } else if(category < 200) { // follow
        return null;
    } else if(category < 300) { // comment
        return null;
    } else {
        return null;
    }
}

Parse.Cloud.define('sendNotification', function(req, res) {
    Parse.initialize("PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2",
    "pEtgTrsGsQKeJzXO4fuxsAL8rZO3svT2ehytb9G5",
    "qmDSAXnLGf6RT16bG1vXqNcmXNe0POaK0bGrtXmV");
    console.log('Request for sending notification ' 
        + JSON.stringify(req.params));
    var nData = req.params;
    
    saveNotification(nData, function(error, notification) {
        if(error) {
          console.log('send notification failed at saving: ' 
            + JSON.stringify(error));
          res.error(error);
        } else {
          console.log('Saved notification: ' + JSON.stringify(notification));
          sendNotification(notification, function(error, sentNotification) {
            if(error) { 
                console.log('Sending notification failed: ' 
                    + JSON.stringify(error));
                res.error(error);
            } else {
//                console.log('Notification sent: ' + JSON.stringify(notificaion));
                res.success(sentNotification);
            }
            });
        }
    });
});

/*
  Send notification
  Only the 'deviceToken' is required
*/
function sendNotification(notification, callback) {
    var pushQuery = new Parse.Query(Parse.Installation);
    pushQuery.equalTo(MODSPOT_ID, notification.get('receiverId'));
//    console.log('!~~~~~! notification: ' + JSON.stringify(notification));
    console.log('!~~~~~! notification: ' + notification.get('receiverId'));
//    var pushQuery = new Parse.Query('UserDevice');
//    pushQuery.equalTo('userId', notification.receiverId);
    console.log('******!!sendNOtification query is ' + JSON.stringify(pushQuery));
//    pushQuery.find({
//        useMasterKey: true,
//        success: function(results) {
//            console.log('found results:' + JSON.stringify(results));
//            callback(null, results);
//        },
//        error: function(error) {
//            console.log(JSON.stringify(error));
//            callback(error);
//        }
//    });
    
    notification.createTime = new Date(notification.createTime);
    Parse.Push.send({
        where: pushQuery,
        data: notification
    }, {
        success: function() {
            console.log('Success sending notification');
            callback(null, notification);
      // response.success({'send' : {'id' : notification.objectId}});
        }, error: function(err) {
            console.log('Sending notification failed' + JSON.stringify(err));
            callback(error);
        }
    }); 
}

// ======= get the newest notifications =========
Parse.Cloud.define('getLatestNotifications', function(req, res) {
    Parse.initialize("PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2",
    "pEtgTrsGsQKeJzXO4fuxsAL8rZO3svT2ehytb9G5",
    "qmDSAXnLGf6RT16bG1vXqNcmXNe0POaK0bGrtXmV");
    console.log('params are: ' + JSON.stringify(req.params));
    var lastTimeString = req.params.lastTime;
    var query = new Parse.Query(NOTIFICATION);
    query.equalTo('receiverId', req.params.userId); // this user
    query.limit(100); // limit
    query.descending('createdAt'); // order by
    if(lastTimeString != undefined) {
        var lastTime = new Date(lastTimeString);
        console.log('last time is ' + lastTime);
        query.greaterThan('createdAt', lastTime);
    }
    query.find({
        success: function(results) {
            console.log('found results: ' + JSON.stringify(results));
            res.success(results);
        },
        error: function(error) {
            console.log('find the lastest notifications failed');
            res.error(error);
        }
    });
});

// ==== get the device token for the user_id ========
Parse.Cloud.define('getDeviceToken', function(req, res) {
    Parse.initialize("PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2",
    "pEtgTrsGsQKeJzXO4fuxsAL8rZO3svT2ehytb9G5",
    "qmDSAXnLGf6RT16bG1vXqNcmXNe0POaK0bGrtXmV");
    
    console.log('Request for get device token ' 
        + JSON.stringify(req.params));
    var userId = req.params.userId;
    console.log('user id is ' + userId);
    
    var query = new Parse.Query(Parse.Installation);
    query.equalTo('modspot_id', userId); 
    query.find({
        useMasterKey: true,
        success: function(array) {
            console.log('array is ' + JSON.stringify(array));
            res.success(array);  
        },
        error: function(error) {
            console.log('error is ' + JSON.stringify(error));
            res.error(error);
        }
    });
});
Parse.Cloud.define('deleteDeviceToken', function(req, res) {
    Parse.initialize("PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2",
    "pEtgTrsGsQKeJzXO4fuxsAL8rZO3svT2ehytb9G5",
    "qmDSAXnLGf6RT16bG1vXqNcmXNe0POaK0bGrtXmV");
    
    console.log('Request for delete device token ' 
        + JSON.stringify(req.params));
    var objId = req.params.objectId;
    console.log('user id is ' + objId);
    
    var query = new Parse.Query(Parse.Installation);
    query.equalTo('objectId', objId); 
    query.first({
        useMasterKey: true,
        success: function(result) {
            console.log('result is ' + JSON.stringify(result));
            result.destroy({
                useMasterKey: true,
                success: function(deleted) {
                    console.log('deleted: ' + JSON.stringify(deleted));
                    res.success('deleted');
                },
                error: function(error) {
                    console.log('delete device token error:' + JSON.stringify(error));
                    res.error(error);
                }
            });
            
        },
        error: function(error) {
            console.log('error is ' + JSON.stringify(error));
            res.error(error);
        }
    });
});
//
//Parse.Cloud.define('getDeviceToken', function(req, res) {
//    Parse.initialize("PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2",
//    "pEtgTrsGsQKeJzXO4fuxsAL8rZO3svT2ehytb9G5",
//    "qmDSAXnLGf6RT16bG1vXqNcmXNe0POaK0bGrtXmV");
//    
//    console.log('Request for get device token ' 
//        + JSON.stringify(req.params));
//    var userId = req.params.userId;
//    console.log('user id is ' + userId);
//    
//    var query = new Parse.Query(Parse.Installation);
//    query.equalTo('modspot_id', userId); 
//    query.find({
//        useMasterKey: true,
//        success: function(array) {
//            console.log('array is ' + JSON.stringify(array));
//            res.success(array);  
//        },
//        error: function(error) {
//            console.log('error is ' + JSON.stringify(error));
//            res.error(error);
//        }
//    });
//});

// ====== delete notification by id =========


// ======= confirm single notification delivery =====
Parse.Cloud.define('notificationDelivered', function(req, res) {
  Parse.initialize("PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2",
    "pEtgTrsGsQKeJzXO4fuxsAL8rZO3svT2ehytb9G5",
    "qmDSAXnLGf6RT16bG1vXqNcmXNe0POaK0bGrtXmV");

  var notification = req.params;
  confirmNotificationDelivery(notification, function(error, saved) {
    if(error) {
      res.error('failed: ' + error);
    } else {
      res.success({'Confirmed: ': saved});
    }
  });
});

function confirmNotificationDelivery(notification, callback) {
  var notificationId = notification.notificationId;
  fetchNotificationById(notificationId, function(err, result) {
    if(err) {
      callback(err);
    } else {
      result.save(null, {
        success: function(savedNotification) {
          savedNotification.set(STATUS, 4);
          savedNotification.save();

          console.log('Notification delivery confirmed: ' 
            + JSON.stringify(savedNotification));
          callback(null, savedNotification);
        },
        error: function(err) {
          console.log('Notification delivery confirmation failed: ' 
            + JSON.stringify(err));
        }
      });
    }
  }); 
}

// ======= confirm batch notifications delivery ========
Parse.Cloud.define('confirmAllNotifications', function(req, res) {
  Parse.initialize("PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2",
    "pEtgTrsGsQKeJzXO4fuxsAL8rZO3svT2ehytb9G5",
    "qmDSAXnLGf6RT16bG1vXqNcmXNe0POaK0bGrtXmV");
  var query = new Parse.Query(NOTIFICATION);
  var ids = req.params.ids;
  if(ids.length > 0) {
    query.containedIn('objectId', ids);
  } else {
    query.equalTo(RECEIVER_ID, req.params.receiverId);
    query.lessThan(STATUS, STATUS_READ);
  }
  console.log('confirm ids are: ' + ids);
  query.find({
    success: function(results) {
      var len = results.length;

      console.log('Found results: ' 
        + JSON.stringify(results));
      results.forEach(function(result, index, allResults) {
        result.save(null, {
          success: function(savedResult) {
            // console.log('1Confirmed notification: ' 
            //   + JSON.stringify(savedResult));
            // console.log('3Confirmed notification: ' 
            //   + savedResult.get('status'));
            savedResult.set(STATUS, 4);
            savedResult.save();

            console.log('Confirmed notification : ' + index +
              + savedResult.get('receiverId') + ' and len is ' + len);
            len -= 1;
            if(len == 0) {
              console.log('Saving all notifications completed');
              res.success({'ok' : 'All delivery confirmed'});
            }
          }, 
          error: function(error) {
            console.log('Error confirming notification: ' 
              + result.get('objectId') + ' due to: ' + JSON.stringify(error));
          }
        });
      });
    },
    error: function(err) {
      console.log('fetching all confirm ids failed: ' 
        + JSON.stringify(err));
      res.error(err);
    }
  });

});

function readAllUnreadNotifications(exceptIds, userId, callback) {
  var query = new Parse.Query(NOTIFICATION);
  query.equalTo(RECEIVER_ID, userId);
  query.lessThan(STATUS, 4);
  query.notContainedIn('objectId', exceptIds);
  
  console.log('notification ids ' + exceptIds 
    + ' and query: ' + query);
  query.find({
    success: function(results) {
      results.forEach(function(result, index, all) {
        result.save(null, {
          success: function(savedResult) {
            savedResult.set(STATUS, 4);
            savedResult.save();
          },
          error: function(error) {
            console.log('ERROR: saving result failed ' + result);
          }
        });
      });
      callback(null);
    },  
    error: function(error) {
      console.log('error' + JSON.stringify(error));
      callback(error);
    }
  });
}


//======= delete old notifications =========
Parse.Cloud.define('deleteOldNotifications', function(req, res) {
  Parse.initialize("PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2",
    "pEtgTrsGsQKeJzXO4fuxsAL8rZO3svT2ehytb9G5",
    "qmDSAXnLGf6RT16bG1vXqNcmXNe0POaK0bGrtXmV");

  var receiverId = req.params.receiverId;
  // delete for older than 1 day
  deleteOldNotifications(60*60*24*7, receiverId, function(error, results) {
    if(error) {
      res.error(error);
    } else {
      res.success({"ok" : "deleted " + results.length + " records"});
    }
  });
});


function deleteOldNotifications(expirationTimeInSec, userId, callback) {
  var query = new Parse.Query(NOTIFICATION);
  query.equalTo(RECEIVER_ID, userId);
  var now = new Date();
  var oldTime = now.getTime() - (expirationTimeInSec * 1000);
  var oldDate = new Date(oldTime);
  query.lessThan('updatedAt', oldDate);
  console.log('now:'+ now +' oldTime:' + oldTime + ' oldDate:' + oldDate);

  query.find({
    success: function(results) {
      // console.log('results are: ' + JSON.stringify(results));
      var len = results.length;
      if(len == 0) {
        callback(null, results);
      } else {
        results.forEach(function(result, index, all) {
          result.destroy({
            success: function(myResult) {
              len -= 1;
              console.log('Success deleting notification: ' 
                + JSON.stringify(myResult) + ' and length is ' + len);
              if(len == 0) {
                callback(null, results);
              }
            },
            error: function(error) {
              console.log('deleting notification: ' 
                + result + ' failed:' + JSON.stringify(error));
              callback(error);
            }
          });
        });
      }
    },
    error: function(error) {
      console.log('error:' + JSON.stringify(error));
      callback(error);
    }

  });

}

//========= Fetch Notification by Id =========
function fetchNotificationById(notificationId, callback) {
  console.log('notification id: ' + notificationId);

  var query = new Parse.Query(NOTIFICATION);
  query.equalTo('objectId', notificationId);
  query.first({
    success: function(notification) {
      console.log('Got the notifiation: ' + JSON.stringify(notification));
      callback(null, notification);
    }, 
    error: function(err) {
      console.log('Fetch notification failed: ' + JSON.stringify(err));
      callback(err);
    }
  });
}

// ========= reset user badge ============
Parse.Cloud.define('resetUserBadge', function(req, res) {
  Parse.initialize("PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2",
    "pEtgTrsGsQKeJzXO4fuxsAL8rZO3svT2ehytb9G5",
    "qmDSAXnLGf6RT16bG1vXqNcmXNe0POaK0bGrtXmV");

  console.log('request: ' + JSON.stringify(req.params));
  resetUserBadge(req.params.userId, function(error, result) {
    if(error) {
      res.error(error);
    } else {
      res.success(result);
    }
  });
}); 

function resetUserBadge(userId, callback) {
  var pushQuery = new Parse.Query(Parse.Installation);
  pushQuery.equalTo('modspot_id', userId); 
  console.log('user id: ' + userId);

  pushQuery.first({useMasterKey:true,
    success: function(result) {
      result.save({
        useMasterKey: true,
        success: function(savedResult) {
          savedResult.set('badge', 0);
          savedResult.save({useMasterKey: true});
          callback(null, savedResult);
        },
        error: function(error) {
          console.log(JSON.stringify(error));
          callback(error);
        }
      });
    },
    error: function(error) {
      console.log(JSON.stringify(error));
      // res.error(error);
      callback(error);
    }
  });
}

function fetchUserById(userId, callback) {
  var pushQuery = new Parse.Query(Parse.Installation);
  pushQuery.equalTo('modspot_id', userId); 
  console.log('user id: ' + userId);

  pushQuery.first({useMasterKey:true,
    success: function(result) {
      console.log(JSON.stringify(result));
      // res.success(result);
      callback(null, result);
    },
    error: function(error) {
      console.log(JSON.stringify(error));
      // res.error(error);
      callback(error);
    }
  });
  // pushQuery.first({
  //   success: function(result) {
  //     console.log(JSON.stringify(result));
  //     // res.success(result);
  //     callback(null, result);
  //   },
  //   error: function(error) {
  //     console.log(JSON.stringify(error));
  //     // res.error(error);
  //     callback(error);
  //   }
  // });
}

// ======= Get unread notifications =========
Parse.Cloud.define('getUnreadNotifications', function(req, res) {
  Parse.initialize("PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2",
    "pEtgTrsGsQKeJzXO4fuxsAL8rZO3svT2ehytb9G5",
    "qmDSAXnLGf6RT16bG1vXqNcmXNe0POaK0bGrtXmV");

  // console.log('request: ' + JSON.stringify(req.params));
  // fetchUserById(req.params.receiverId, function(error, result) {
  //   if(error) {
  //     res.error(error);
  //   } else {
  //     res.success(result);
  //   }

  // });

  // var notification = {receiverId: req.params.receiverId};

  fetchUserUnreadNotifications(req.params, function(err, results) {
    if(err) {
      res.error('fetchUserUnreadNotifications failed: ' + err);
    } else {
      console.log('fetched: ' + JSON.stringify(results));
      res.success(results);
    }
  });
}); 

function fetchUserUnreadNotifications(notification, callback) {
  var query = new Parse.Query(NOTIFICATION);
  query.equalTo(RECEIVER_ID, notification.receiverId);
  query.lessThan(STATUS, 4);
  query.descending(UPDATE_TIME);
  query.limit(UNREAD_NOTIFICATION_LIMIT);

  query.find({
    success: function(results) {
      console.log('Fetch unread notification: ' + JSON.stringify(results));
      callback(null, results);
    },
    error: function(err) {
      console.log('Fetch unread notifications error: ' + err);
      callback(err);
    }
  });
}

// ===== invalidate all unread notifications
function readUnreadNotifications(notification, callback) {
  var query = new Parse.Query(NOTIFICATION);
  query.equalTo(RECEIVER_ID, notification.receiverId);
  query.equalTo(STATUS, 0);

  query.descending(UPDATE_TIME);
  query.skip(UNREAD_NOTIFICATION_LIMIT);

  query.find({
    success: function(results) {
      var len = results.length;
      if(len > 0) {
        results.forEach(function(result, index, all) {
          result.save({
            success: function(savedResult) {
              savedResult.set(STATUS, STATUS_READ);
              savedResult.save();
              len -= 1;
              if(len == 0) {
                callback(null, results);
              }
            },
            error: function(error) {
              callback(error);
            }
          });
        });
      }
      console.log('Fetch unread notification: ' + JSON.stringify(results));
      callback(null, results);
    },
    error: function(err) {
      console.log('Fetch unread notifications error: ' + err);
      callback(err);
    }
  });
}


Parse.Cloud.beforeSave('Notification', function(request, response) {
  console.log('Ran beforeSave on objectId: ' + request.object.id);

  response.success();
});

Parse.Cloud.afterSave('Notification', function(request, response) {
  console.log('Ran afterSave on objectId: ' + request.object.id);

});

Parse.Cloud.beforeDelete('Notification', function(request, response) {
  console.log('Ran beforeDelete on objectId: ' + request.object.id);
  response.success();
});

Parse.Cloud.afterDelete('Notification', function(request, response) {
  console.log('Ran afterDelete on objectId: ' + request.object.id);
});

