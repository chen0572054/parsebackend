## Migrating your Cloud Code to Heroku

Take your existing cloud code files and place them in the `cloud/` folder.  Add the following line at the very top of each file:

```
var Parse = require('parse-cloud-express').Parse;
```

Set up the necessary details in your Heroku config and deploy:

```
heroku config:set HOOKS_URL=yourherokuurl
heroku config:set PARSE_APP_ID=yourappid
heroku config:set PARSE_MASTER_KEY=yourmasterkey
heroku config:set PARSE_WEBHOOK_KEY=yourkeyhere

git push heroku master
```

A post-install script (`scripts/register-webhooks.js`) will enumerate your webhooks and register them with Parse.

### Caveats

Cloud Code required you to use `cloud/` as a prefix for all other .js files, even though they were in the same folder.  That doesn't apply here, so you'll need to update any require statements in files under `cloud/` to reference just `./` instead.

The first-party modules hosted by Parse will not be available (sendgrid, mailgun, stripe, image, etc.) and you'll need to update your code to use the official modules available via npm.

The base mount path is set in both `server.js` and `scripts/register-webhooks.js` and must be equal.


//===== working curl example of backend api

// send notification
curl -X POST -H "X-Parse-Application-Id: PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2"  -H "X-Parse-REST-API-Key: qOu3iYM078v4sjIhM0DNSo1tpwNRMLQi06q3xyAU"  -H "Content-Type: application/json" -d '{"senderId": "54dec6673500005c004af0f5", "receiverId":"54dec6673500005c004af0f5", "alert" : "ccc liked your post", "category" : 2, "status":0, "createTime" : "2016-01-26T22:27:24.837Z", "updateTime" : "2016-01-26T22:27:24.837Z", "postId" : "56a8511f1300004b1a3c2df8", "sound":"Default", "badge" : "Increment"}'  https://api.parse.com/1/functions/sendNotification


// get unread notifications
curl -X POST -H "X-Parse-Application-Id: PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2"  -H "X-Parse-REST-API-Key: qOu3iYM078v4sjIhM0DNSo1tpwNRMLQi06q3xyAU" -H "Content-Type: application/json" -d '{"receiverId" : "54dec6673500005c004af0f5"}' https://api.parse.com/1/functions/getUnreadNotifications

// confirm notification delivery, NOTE: deprecated
curl -X POST -H "X-Parse-Application-Id: PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2"  -H "X-Parse-REST-API-Key: qOu3iYM078v4sjIhM0DNSo1tpwNRMLQi06q3xyAU" -H "Content-Type: application/json" -d '{"notificationId": "5gcPf8qZxp"}'  https://api.parse.com/1/functions/notificationDelivered

// get a single notification
curl -X GET -H "X-Parse-Application-Id: PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2"  -H "X-Parse-REST-API-Key: qOu3iYM078v4sjIhM0DNSo1tpwNRMLQi06q3xyAU" https://api.parse.com/1/classes/Notification/5gcPf8qZxp

// get all the notifications 
curl -X GET -H "X-Parse-Application-Id: PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2"  -H "X-Parse-REST-API-Key: qOu3iYM078v4sjIhM0DNSo1tpwNRMLQi06q3xyAU" https://api.parse.com/1/classes/Notification

// confirm all notifications, NOTE: deprecated
curl -X POST -H "X-Parse-Application-Id: PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2"  -H "X-Parse-REST-API-Key: qOu3iYM078v4sjIhM0DNSo1tpwNRMLQi06q3xyAU" -H "Content-Type: application/json" -d '{"ids": ["5gcPf8qZxp", "xlopiRpPk5","vM5YbXXHFM","B0ZGDy3Vfl","BMJYJQLtu2","dF5Kdr0YOJ"]}'  https://api.parse.com/1/functions/confirmAllNotifications


// get unread Notifications: NOTE: deprecated
curl -X POST -H "X-Parse-Application-Id: PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2"  -H "X-Parse-REST-API-Key: qOu3iYM078v4sjIhM0DNSo1tpwNRMLQi06q3xyAU" -H "Content-Type: application/json" -H "X-Parse-Master-Key:qmDSAXnLGf6RT16bG1vXqNcmXNe0POaK0bGrtXmV" -d '{"receiverId" : "55c7edde140000051df2a2c8"}' https://api.parse.com/1/functions/getUnreadNotifications

// reset user's badge to 0 
curl -X POST -H "X-Parse-Application-Id: PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2"  -H "X-Parse-REST-API-Key: qOu3iYM078v4sjIhM0DNSo1tpwNRMLQi06q3xyAU" -H "Content-Type: application/json" -H "X-Parse-Master-Key:qmDSAXnLGf6RT16bG1vXqNcmXNe0POaK0bGrtXmV" -d '{"userId" : "54dec6673500005c004af0f5"}' https://api.parse.com/1/functions/resetUserBadge


/*
For parse Installation table, there are attributes:
1. modspot_id 
2. deviceId
3. deviceToken
*/
// ======= get user device token =======
curl -X POST -H "X-Parse-Application-Id: PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2"  -H "X-Parse-REST-API-Key: qOu3iYM078v4sjIhM0DNSo1tpwNRMLQi06q3xyAU" -H "Content-Type: application/json" -H "X-Parse-Master-Key:qmDSAXnLGf6RT16bG1vXqNcmXNe0POaK0bGrtXmV" -d '{"userId" : "55c7edde140000051df2a2c8"}' https://api.parse.com/1/functions/getDeviceToken

/*
* parse bug: all the functions outside of main.js are not recognized.
Solution: register the functions in main.js and then move them to separate file.

parse bug: when an object is saved/deleted, must call beforeSave afterSave, beforeDelete and afterDelete. If there methods are not available, then the saving will fail giving error code : 142, validation failed
*/

// query Installation
curl -X GET -H "X-Parse-Application-Id: PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2"  -H "X-Parse-REST-API-Key: qOu3iYM078v4sjIhM0DNSo1tpwNRMLQi06q3xyAU" -H "Content-Type: application/json" https://api.parse.com/1/classes/Installation

// get user device token for given userId(modspot_id)
curl -X POST -H "X-Parse-Application-Id: PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2"  -H "X-Parse-REST-API-Key: qOu3iYM078v4sjIhM0DNSo1tpwNRMLQi06q3xyAU" -H "Content-Type: application/json" -H "X-Parse-Master-Key:qmDSAXnLGf6RT16bG1vXqNcmXNe0POaK0bGrtXmV" -d '{"userId" : "55c7edde140000051df2a2c8"}' https://api.parse.com/1/functions/getDeviceToken

// delete
curl -X POST -H "X-Parse-Application-Id: PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2"  -H "X-Parse-REST-API-Key: qOu3iYM078v4sjIhM0DNSo1tpwNRMLQi06q3xyAU" -H "Content-Type: application/json" -H "X-Parse-Master-Key:qmDSAXnLGf6RT16bG1vXqNcmXNe0POaK0bGrtXmV" -d '{"objectId" : "VP6rvp0gTX"}' https://api.parse.com/1/functions/deleteDeviceToken

/*
get the last 100 notifications fro user
 If "lastTime" was provided, the only get the notifications after this time(exclusive)
*/ 
curl -X POST -H "X-Parse-Application-Id: PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2"  -H "X-Parse-REST-API-Key: qOu3iYM078v4sjIhM0DNSo1tpwNRMLQi06q3xyAU" -H "Content-Type: application/json" -d '{"userId" : "55c7edde140000051df2a2c8", "lastTime":"2016-02-07T21:05:28.294Z"}' https://api.parse.com/1/functions/getLatestNotifications

/*
Call this login endpoint to register a modspot_id with deviceId and deviceToken
*/
curl -X POST -H "X-Parse-Application-Id: PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2"  -H "X-Parse-REST-API-Key: qOu3iYM078v4sjIhM0DNSo1tpwNRMLQi06q3xyAU" -H "Content-Type: application/json" -d '{"userId" : "111", "deviceToken":"aaa", "deviceId":"123abc"}' https://api.parse.com/1/functions/userLoginDevice
/*
Call this logout endpoint to delete the modspot_id and deviceToken entry
*/
curl -X POST -H "X-Parse-Application-Id: PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2"  -H "X-Parse-REST-API-Key: qOu3iYM078v4sjIhM0DNSo1tpwNRMLQi06q3xyAU" -H "Content-Type: application/json" -d '{"userId" : "111", "deviceToken":"newToken", "deviceId":"123abc"}' https://api.parse.com/1/functions/userLogoutDevice

/*
Two more examples of sendNotification endpoint
*/
curl -X POST -H "X-Parse-Application-Id: PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2"  -H "X-Parse-REST-API-Key: qOu3iYM078v4sjIhM0DNSo1tpwNRMLQi06q3xyAU"  -H "Content-Type: application/json" -d '{"alert":"tracyli liked your post","badge":"Increment","category":1,"createTime":1454281051771,"objectId":"o36UoFHDSd","postId":"56ae5b751300009103041e9c","receiverId":"567097261100001242f720c1","senderId":"54e13de1120000a107b1fb2e","senderName":"tracyli","sound":"Default","status":2}'  https://api.parse.com/1/functions/sendNotification


curl -X POST -H "X-Parse-Application-Id: PhBcZXZ5lvGKblGVBa1GczOd5ZTs7aZatIzECyp2"  -H "X-Parse-REST-API-Key: qOu3iYM078v4sjIhM0DNSo1tpwNRMLQi06q3xyAU"  -H "Content-Type: application/json" -d '{"senderId": "54dec6673500005c004af0f5", "receiverId":"54dec6673500005c004af0f5", "alert" : "No Installation", "category" : 2, "status":0, "createTime" : 1453847244837, "updateTime" : 1453847244837, "postId" : "56a8511f1300004b1a3c2df8", "senderName":"Helenh", "sound":"Default", "badge" : "Increment"}'  https://api.parse.com/1/functions/sendNotification



